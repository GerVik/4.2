# 4.2


|Nagłówek 1|Nagłówek 2|Nagłówek 3|
|:------------:|:-------------:|:---------:|
|el1|el2|el3 lo [^1]|

# Hello :smiley:

```py
a = 1
b = 2
sum = a + b
print (sum)
```

[^1]: To jest footnote.
# Good :blush:
[Coś tutaj jest](#Hello)

# Plany na jutro :wink: 

- [x] Wstać
- [ ] Zrobić zadania z Warsztatu
